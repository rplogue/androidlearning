package logue.robert.dayinformation.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DayInformationDBHelper extends SQLiteOpenHelper {

	public static final String TABLE_NAME = "dayinformation";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_DATE = "date";
	public static final String COLUMN_INFORMATION = "information";

	private static final String DATABASE_NAME = TABLE_NAME+".db";
	private static final int DATABASE_VERSION = 1;

	
	private static final String DATABASE_CREATE = "create table "
		      + TABLE_NAME + "(" + COLUMN_ID
		      + " integer primary key autoincrement, "
		      + COLUMN_DATE + " text not null, "
		      + COLUMN_INFORMATION
		      + " text not null);";


	public DayInformationDBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
	}

}
