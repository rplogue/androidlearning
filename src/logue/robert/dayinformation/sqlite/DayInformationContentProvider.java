package logue.robert.dayinformation.sqlite;

import logue.robert.dayinformation.sqlite.DayInformationDBHelper;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class DayInformationContentProvider extends ContentProvider{

	private DayInformationDBHelper dbHelper;
	private SQLiteDatabase database = null;
	
	private static final String AUTHORITY = "logue.robert.dayinformation.sqlite.DayInformationContentProvider";
	
	private static final String DAYINFORMATION_BASE_PATH = "DayInformation";
	public static final Uri DAYINFORMATION_CONTENT_URI = Uri.parse("content://" + AUTHORITY
	        + "/" + DAYINFORMATION_BASE_PATH);
	
	public static final int INFORMATION = 1;
	public static final int INFORMATION_ID = 2;	
	
	
	private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	
	static 
	{
	    sURIMatcher.addURI(AUTHORITY, DAYINFORMATION_BASE_PATH, INFORMATION);
	    sURIMatcher.addURI(AUTHORITY, DAYINFORMATION_BASE_PATH + "/#", INFORMATION_ID);
	}
	
	@Override
	public int delete(Uri uri, String where, String[] selectionArgs) {
		
		int uriType = sURIMatcher.match(uri);
		int rowsAffected = 0;
		
		//Only handle delete single row case
		switch(uriType){
		
		case INFORMATION_ID:
			if(database == null){
				database = dbHelper.getWritableDatabase();
			}
			rowsAffected = database.delete(DayInformationDBHelper.TABLE_NAME, DayInformationDBHelper.COLUMN_ID+"=?",
			          new String[] { uri.getLastPathSegment() });
	      
			break;
		default:
	        throw new IllegalArgumentException("Unknown URI");
		
		}
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		
		int uriType = sURIMatcher.match(uri);
		long rowID = 0;
		StringBuffer returnUri = null;
		
		switch (uriType) {
		
		case INFORMATION:
		
			if(database == null){
				database = dbHelper.getWritableDatabase();
			}
			
			rowID = database.insert(DayInformationDBHelper.TABLE_NAME, null, values);
			break;
		default:
	        throw new IllegalArgumentException("Unknown URI");
		}
		
		if(rowID > 0)
		{
			returnUri = new StringBuffer(uri.toString());
			returnUri.append("/"+rowID);
			
			return Uri.parse(returnUri.toString());
		}
	    
		return null;
	}

	@Override
	public boolean onCreate() {
		dbHelper = new DayInformationDBHelper(getContext());
	
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
	    queryBuilder.setTables(DayInformationDBHelper.TABLE_NAME);
	    
	    int uriType = sURIMatcher.match(uri);
	    
	    switch (uriType) {
	    case INFORMATION_ID:
	    	queryBuilder.appendWhere(DayInformationDBHelper.COLUMN_ID + "=" +uri.getLastPathSegment());
	    	
	        break;
	    case INFORMATION:
	        // no filter necessary
	        break;
	    default:
	        throw new IllegalArgumentException("Unknown URI");
	    }
	    
	    if(database == null){
			database = dbHelper.getWritableDatabase();
		}
	    
	    Cursor cursor = queryBuilder.query(database,
	            projection, selection, selectionArgs, null, null, sortOrder);
	    
	    cursor.setNotificationUri(getContext().getContentResolver(), uri);
	    
	    return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		
		int uriType = sURIMatcher.match(uri);
		int rowID = 0;
		
		if(database == null){
			database = dbHelper.getWritableDatabase();
		}
		
		switch (uriType) {
		
		case INFORMATION:		
			
			rowID = database.update(DayInformationDBHelper.TABLE_NAME, values,selection,selectionArgs);
			break;
		default:
	        throw new IllegalArgumentException("Unknown URI");
		}
		
		return rowID;
	}

}
