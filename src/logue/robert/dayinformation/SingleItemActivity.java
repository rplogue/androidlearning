package logue.robert.dayinformation;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

//import logue.robert.dayinformation.EditSingleItemActivity;
import logue.robert.dayinformation.MainActivity;
import logue.robert.dayinformation.R;
import logue.robert.dayinformation.sqlite.DayInformationContentProvider;
import logue.robert.dayinformation.sqlite.DayInformationDBHelper;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class SingleItemActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final String ID = "ID";
	private static final int LOADER_ID = 0;
	private long itemID;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_single_item);
		
		itemID = getIntent().getExtras().getLong(ID, -1);
		
		Log.i("SingleItem","RL single item id "+itemID);
		
		Bundle params = new Bundle();

		params.putLong(ID,itemID);
		
		getSupportLoaderManager().initLoader(LOADER_ID, params, this);	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_single_item, menu);
		return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle bundle) {
		Log.i("SingleItem","RL onCreateLoader");
		String[] projection = {DayInformationDBHelper.COLUMN_ID,DayInformationDBHelper.COLUMN_DATE
				,DayInformationDBHelper.COLUMN_INFORMATION};
		
		return new CursorLoader(this,Uri.parse(DayInformationContentProvider.DAYINFORMATION_CONTENT_URI+"/"+bundle.getLong(ID)), projection
									, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor item) {
		if(item != null && item.getCount() > 0)
		{
			item.moveToFirst();
			
			long time = item.getLong(item.getColumnIndex(DayInformationDBHelper.COLUMN_DATE))* 1000L;;
			
			if(time > 0)
			{
				Calendar cal = Calendar.getInstance();
		        cal.setTimeInMillis(time);
		        
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.UK);
				String stringDate = formatter.format(cal.getTime());
				
				TextView showDate = (TextView)findViewById(R.id.show_date);
				showDate.setText(stringDate);
			}
			
			String information = item.getString(item.getColumnIndex(DayInformationDBHelper.COLUMN_INFORMATION));
			if(information != null)
			{
				TextView showInformation = (TextView)findViewById(R.id.show_all_information);
				
				showInformation.setText(information);		
			}
		
		}
		
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		
	}

	public void onEditClick(View view)
	{
		Intent myIntent = new Intent(getApplicationContext(), EditSingleItemActivity.class);
	    Bundle params = new Bundle();

		params.putLong(ID,itemID);

		myIntent.putExtras(params);
		startActivity(myIntent);
	}
	
	public void onDeleteClick(View view)
	{
		getContentResolver().delete(Uri.parse(DayInformationContentProvider.DAYINFORMATION_CONTENT_URI+"/"+itemID), "", null);
		
		getContentResolver().notifyChange(DayInformationContentProvider.DAYINFORMATION_CONTENT_URI, null);
		
		Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
	    
		startActivity(myIntent);
	}

}
