package logue.robert.dayinformation;

import logue.robert.dayinformation.R;
import logue.robert.dayinformation.sqlite.DayInformationContentProvider;
import logue.robert.dayinformation.sqlite.DayInformationDBHelper;

import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class EditSingleItemActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final String ID = "ID";
	private static final int LOADER_ID = 0;
	private long itemID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_single_item);
		
		itemID = getIntent().getExtras().getLong(ID, -1);
		
		Log.i("EditSingleItemActivity","RL id "+itemID);
		
		Bundle params = new Bundle();

		params.putLong(ID,itemID);
		
		getSupportLoaderManager().initLoader(LOADER_ID, params, this);	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_edit_single_item, menu);
		return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle bundle) {
		Log.i("SingleItem","RL onCreateLoader");
		String[] projection = {DayInformationDBHelper.COLUMN_ID,DayInformationDBHelper.COLUMN_DATE
				,DayInformationDBHelper.COLUMN_INFORMATION};
		
		return new CursorLoader(this,Uri.parse(DayInformationContentProvider.DAYINFORMATION_CONTENT_URI+"/"+bundle.getLong(ID)), projection
									, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor item) {
		if(item != null && item.getCount() > 0)
		{
			item.moveToFirst();
			
			String information = item.getString(item.getColumnIndex(DayInformationDBHelper.COLUMN_INFORMATION));
			if(information != null)
			{
				EditText showInformation = (EditText)findViewById(R.id.edit_all_information);
				
				showInformation.setText(information);		
			}
		
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void onSaveClick(View view)
	{
		Log.i("SingleItem","RL onSaveClick itemID = "+itemID);
		
		EditText information = (EditText)findViewById(R.id.edit_all_information);
		
		ContentValues values = new ContentValues();
		
		values.put(DayInformationDBHelper.COLUMN_INFORMATION, information.getText().toString());
		
		String whereClause =  DayInformationDBHelper.COLUMN_ID + "=?";
		String[] args = {new Long(itemID).toString()};
		
		getContentResolver().update(DayInformationContentProvider.DAYINFORMATION_CONTENT_URI, values, whereClause,args);
		
		getContentResolver().notifyChange(DayInformationContentProvider.DAYINFORMATION_CONTENT_URI, null);
		
		finish();
	}
	
	public void onCancelClick(View view)
	{
		finish();
	}

}
