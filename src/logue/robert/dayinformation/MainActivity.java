package logue.robert.dayinformation;


import logue.robert.dayinformation.SingleItemActivity;

import logue.robert.dayinformation.sqlite.DateFormatAdapter;

import logue.robert.dayinformation.R;

import logue.robert.dayinformation.sqlite.DayInformationContentProvider;
import logue.robert.dayinformation.sqlite.DayInformationDBHelper;

import android.os.Bundle;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

public class MainActivity extends FragmentActivity implements LoaderManager.LoaderCallbacks<Cursor> {

	private CursorAdapter adapter;
	
	private static final int LOADER_ID = 0x01;
	private static final String ID = "ID";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i("MainActivity","RL onCreate");
		setContentView(R.layout.activity_main);	
		
		getSupportLoaderManager().initLoader(LOADER_ID, null, this);
		
		adapter = new DateFormatAdapter(this,null);
		
		 ListView listView = (ListView) findViewById(R.id.list);
	     listView.setAdapter(adapter);
	     
	     listView.setClickable(true);

	     listView.setOnItemClickListener(new AdapterView.OnItemClickListener() 
		{
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
				Log.i("MainActivity","RL click click pos "+position+" id "+id);
				
				Intent myIntent = new Intent(getApplicationContext(), SingleItemActivity.class);
			    Bundle params = new Bundle();

				params.putLong(ID,id);
	
				myIntent.putExtras(params);
				startActivity(myIntent);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void onClick(View view){
		Log.i("MainActivity","RL onClick");
		switch(view.getId()){
			case R.id.add:
				EditText information = (EditText)findViewById(R.id.write_information);
				
				ContentValues values = new ContentValues();
				
				values.put(DayInformationDBHelper.COLUMN_DATE, System.currentTimeMillis()/1000);
				values.put(DayInformationDBHelper.COLUMN_INFORMATION, information.getText().toString());
				
				getContentResolver().insert(DayInformationContentProvider.DAYINFORMATION_CONTENT_URI, values);
				
				information.setText("");
				
				getContentResolver().notifyChange(DayInformationContentProvider.DAYINFORMATION_CONTENT_URI, null);
				
				break;
		}
		
	}

	@Override
	public Loader<Cursor> onCreateLoader(int i, Bundle bundle)
	{
		Log.i("MainActivity","RL onCreateLoader");
		String[] projection = {DayInformationDBHelper.COLUMN_ID,DayInformationDBHelper.COLUMN_DATE
				,DayInformationDBHelper.COLUMN_INFORMATION};

	    return new CursorLoader(this,DayInformationContentProvider.DAYINFORMATION_CONTENT_URI
	                , projection, null, null, DayInformationDBHelper.COLUMN_DATE +" DESC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) 
	{
		Log.i("MainActivity","RL onLoadFinished");
		  adapter.swapCursor(cursor);
	}
	
	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) 
	{
		Log.i("MainActivity","RL onLoaderReset");
		  adapter.swapCursor(null);
	}
		

}

